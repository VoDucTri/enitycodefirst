﻿namespace EnityRelationshipDiagram.Models
{
    public class Account
    {
        public int AccountID { get; set; }
        public string AccountName { get; set; }
        public int CustomerID { get; set; }

        public Customer Customer { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
    }
}
