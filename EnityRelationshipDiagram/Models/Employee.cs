﻿using System.ComponentModel.DataAnnotations;

namespace EnityRelationshipDiagram.Models
{
    public class Employee
    {
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ContactAndAddress { get; set; }

        public ICollection<Transaction> Transactions { get; set; }
    }
}
