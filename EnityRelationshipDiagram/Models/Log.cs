﻿namespace EnityRelationshipDiagram.Models
{
    public class Log
    {
        public int LogID { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime LoginTime { get; set; }
        public int EmployeeID { get; set; }
        public int TransactionalID { get; set; }

        public Employee Employee { get; set; }
        public  Transaction Transaction { get; set; }
    }
}
