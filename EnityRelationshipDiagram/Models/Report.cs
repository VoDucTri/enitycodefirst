﻿namespace EnityRelationshipDiagram.Models
{
    public class Report
    {
        public int ReportID { get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate { get; set; }

        public ICollection<Log> Logs { get; set; }
    }
}
